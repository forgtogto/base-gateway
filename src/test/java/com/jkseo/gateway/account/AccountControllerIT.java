package com.jkseo.gateway.account;

import com.jkseo.gateway.IntegrationTest;
import com.jkseo.gateway.repository.AuthorityRepository;
import com.jkseo.gateway.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureWebClient;
import org.springframework.test.web.reactive.server.WebTestClient;

import static com.jkseo.gateway.account.AccountControllerIT.TEST_USER_LOGIN;

@AutoConfigureWebClient
@WithMockUser(value = TEST_USER_LOGIN)
@IntegrationTest
public class AccountControllerIT {

    static final String TEST_USER_LOGIN = "test";

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AuthorityRepository authorityRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private WebTestClient accountWebTestClient;

 


}
