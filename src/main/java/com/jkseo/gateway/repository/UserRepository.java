package com.jkseo.gateway.repository;

import com.jkseo.gateway.domain.User;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
public interface UserRepository extends R2dbcRepository<User, Long>, UserRepositoryInternal {


    Mono<User> findOneByLogin(String login);

    Mono<User> findOneByEmailIgnoreCase(String email);

    @Query("INSERT INTO tr_user_authority VALUES (:userId, :authority)")
    Mono<Void> saveUserAuthority(Long userId, String authority);
}


interface DeleteExtended<T> {
}

interface UserRepositoryInternal extends DeleteExtended<User> {

    Mono<User> findOneWithAuthoritiesByLogin(String login);

    Mono<User> findOneWithAuthoritiesByEmailIgnoreCase(String email);


}
