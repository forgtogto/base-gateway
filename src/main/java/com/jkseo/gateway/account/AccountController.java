package com.jkseo.gateway.account;

import com.jkseo.gateway.MailService;
import com.jkseo.gateway.account.exception.InvalidPasswordException;
import com.jkseo.gateway.account.model.ManagedUserModel;
import com.jkseo.gateway.repository.UserRepository;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import javax.validation.Valid;

@RestController
@RequestMapping("/api")
public class AccountController {

    private static class AccountResourceException extends RuntimeException {

        private AccountResourceException(String message) {
            super(message);
        }
    }

    private final Logger log = LoggerFactory.getLogger(AccountController.class);

    private final UserRepository userRepository;

    private final UserService userService;

    private final MailService mailService;

    public AccountController(UserRepository userRepository, UserService userService, MailService mailService) {
        this.userRepository = userRepository;
        this.userService = userService;
        this.mailService = mailService;
    }


    @PostMapping("/register")
    @ResponseStatus(HttpStatus.CREATED)
    public Mono<Void> registerAccount(@Valid @RequestBody ManagedUserModel managedUserModel) {
        if (isPasswordLengthInvalid(managedUserModel.getPassword())) {
            throw new InvalidPasswordException();
        }
        return userService.registerUser(managedUserModel, managedUserModel.getPassword()).doOnSuccess(mailService::sendActivationEmail).then();
    }

    private static boolean isPasswordLengthInvalid(String password) {
        return (
                StringUtils.isEmpty(password) ||
                        password.length() < ManagedUserModel.PASSWORD_MIN_LENGTH ||
                        password.length() > ManagedUserModel.PASSWORD_MAX_LENGTH
        );
    }
}
