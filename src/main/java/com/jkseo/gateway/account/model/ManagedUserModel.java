package com.jkseo.gateway.account.model;

import com.jkseo.gateway.account.dto.AdminUserDTO;

import javax.validation.constraints.Size;

public class ManagedUserModel extends AdminUserDTO {

    public static final int PASSWORD_MIN_LENGTH = 4;

    public static final int PASSWORD_MAX_LENGTH = 100;

    @Size(min = PASSWORD_MIN_LENGTH, max = PASSWORD_MAX_LENGTH)
    private String password;

    public ManagedUserModel() {
        // Empty constructor needed for Jackson.
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "ManagedUserModel{" +
                "password='" + password + '\'' +
                '}';
    }
}
