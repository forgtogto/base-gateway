package com.jkseo.gateway.account.exception;

import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

import java.net.URI;

public class InvalidPasswordException extends AbstractThrowableProblem {

    private static final long serialVersionUID = 1L;

    public InvalidPasswordException() {
        super(URI.create("/error"), "Incorrect password", Status.BAD_REQUEST);
    }
}
