package com.jkseo.gateway.account.exception;

import java.net.URI;

public class LoginAlreadyUsedException extends BadRequestAlertException {

    private static final long serialVersionUID = 1L;

    public LoginAlreadyUsedException() {
        super(URI.create("/error"), "Login name already used!", "userManagement", "userexists");
    }
}
