package com.jkseo.gateway.account.exception;


import java.net.URI;

public class EmailAlreadyUsedException extends BadRequestAlertException {

    private static final long serialVersionUID = 1L;

    public EmailAlreadyUsedException() {
        super(URI.create("/error"), "Email is already in use!", "userManagement", "emailexists");
    }
}

