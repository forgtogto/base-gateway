package com.jkseo.gateway.account;

import com.jkseo.gateway.account.constants.AuthoritiesConstants;
import com.jkseo.gateway.account.dto.AdminUserDTO;
import com.jkseo.gateway.account.exception.EmailAlreadyUsedException;
import com.jkseo.gateway.account.exception.UsernameAlreadyUsedException;
import com.jkseo.gateway.domain.Authority;
import com.jkseo.gateway.domain.User;
import com.jkseo.gateway.repository.AuthorityRepository;
import com.jkseo.gateway.repository.UserRepository;
import com.jkseo.gateway.util.RandomUtil;
import com.jkseo.gateway.util.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.util.HashSet;
import java.util.Set;

@Service
public class UserService {

    private final Logger log = LoggerFactory.getLogger(UserService.class);

    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    private final AuthorityRepository authorityRepository;

    public UserService(UserRepository userRepository, PasswordEncoder passwordEncoder, AuthorityRepository authorityRepository) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.authorityRepository = authorityRepository;
    }


    @Transactional
    public Mono<User> registerUser(AdminUserDTO userDTO, String password) {
        return userRepository
                .findOneByLogin(userDTO.getLogin().toLowerCase())
                .flatMap(existingUser -> {
                    if (!existingUser.isActivated()) {
                        return userRepository.delete(existingUser);
                    } else {
                        return Mono.error(new UsernameAlreadyUsedException());
                    }
                })
                .then(userRepository.findOneByEmailIgnoreCase(userDTO.getEmail()))
                .flatMap(existingUser -> {
                    if (!existingUser.isActivated()) {
                        return userRepository.delete(existingUser);
                    } else {
                        return Mono.error(new EmailAlreadyUsedException());
                    }
                })
                .publishOn(Schedulers.boundedElastic())
                .then(
                        Mono.fromCallable(() -> {
                            User newUser = new User();
                            String encryptedPassword = passwordEncoder.encode(password);
                            newUser.setLogin(userDTO.getLogin().toLowerCase());
                            newUser.setPassword(encryptedPassword);
                            newUser.setFirstName(userDTO.getFirstName());
                            newUser.setLastName(userDTO.getLastName());
                            if (userDTO.getEmail() != null) {
                                newUser.setEmail(userDTO.getEmail().toLowerCase());
                            }
                            newUser.setImageUrl(userDTO.getImageUrl());
                            newUser.setLangKey(userDTO.getLangKey());
                            // new user is not active
                            newUser.setActivated(false);
                            // new user gets registration key
                            newUser.setActivationKey(RandomUtil.generateActivationKey());
                            return newUser;
                        })
                )
                .flatMap(newUser -> {
                    Set<Authority> authorities = new HashSet<>();
                    return authorityRepository
                            .findById(AuthoritiesConstants.USER)
                            .map(authorities::add)
                            .thenReturn(newUser)
                            .doOnNext(user -> user.setAuthorities(authorities))
                            .flatMap(this::saveUser)
                            .doOnNext(user -> log.debug("Craeted Information for User: {}", user));
                });

    }


    @Transactional
    public Mono<User> saveUser(User user) {
        return SecurityUtils
                .getCurrentUserLogin()
                .switchIfEmpty(Mono.just("system"))
                .flatMap(login -> {
                    if (user.getCreatedBy() == null) {
                        user.setCreatedBy(login);
                    }
                    user.setLastModifiedBy(login);

                    return userRepository
                            .save(user)
                            .flatMap(savedUser ->
                                    Flux
                                            .fromIterable(user.getAuthorities())
                                            .flatMap(authority -> userRepository.saveUserAuthority(savedUser.getId(), authority.getName()))
                                            .then(Mono.just(savedUser))
                            );
                });
    }

}
